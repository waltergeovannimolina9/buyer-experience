---
    title: GitLab for Ops
    description: GitLab is not just an ops tool, it's a tool for ops teams.
    topic_name: GitLab for Ops
    icon: cycle
    date_published: 2022-02-14
    date_modified: 2023-04-20
    topics_header:
      data:
        title: GitLab for Ops
        block:
          - metadata:
              id_tag: gitops
            text: |
             A DevOps platform combines the ability to develop, secure, and operate software in a single application so everyone involved in the software development process - from a product manager to an ops pro - can seamlessly work together to release software faster.
    crumbs:
      - title: Topics
        href: /topics/
        data_ga_name: topics
        data_ga_location: breadcrumb
      - title: GitLab for Ops
    topics_header:
        data:
          title: GitLab for Ops
          block:
            - metadata:
                id_tag: gitlab-for-ops
              text: |
                GitLab is not just an ops tool, it's a tool for ops teams. Learn how your operations personnel can use GitLab for monitoring live applications, managing Kubernetes environments, and incident response.
              link_text: Watch a GitLab demo →
              link_href: /demo/
              data_ga_name: watch a demo
              data_ga_location: header
    crumbs:
      - title: Topics
        href: /topics/
        data_ga_name: topics
        data_ga_location: breadcrumb
      - title: GitLab for Ops
    side_menu:
      anchors:
        text: "On this page"
        data:
          - text: GitLab for Ops teams
            href: "#git-lab-for-ops-teams"
            data_ga_name: GitLab for Ops teams
            data_ga_location: side-navigation
            variant: primary
          - text: Monitor Kubernetes Environments
            href: "#monitor-kubernetes-environments"
            data_ga_name: Monitor Kubernetes Environments
            data_ga_location: side-navigation
          - text: Use Auto-generated pipelines
            href: "#use-auto-generated-pipelines"
            data_ga_name: Use Auto-generated pipelines
            data_ga_location: side-navigation
          - text: View all pipelines at a glance
            href: "#view-all-pipelines-at-a-glance"
            data_ga_name: View all pipelines at a glance
            data_ga_location: side-navigation
          - text: Incident Management
            href: "#incident-management"
            data_ga_name: Incident Management
            data_ga_location: side-navigation
          - text: Serverless
            href: "#serverless"
            data_ga_name: Serverless
            data_ga_location: side-navigation
      content:
        - name: topics-copy-block
          data:
            column_size: 10
            header: GitLab for Ops teams
            blocks:
              - text: |
                  GitLab has long been a favorite of Dev teams, but GitLab is for Ops too. A variety of operations personnel from Sys Admins and IT Ops Engineers to SREs and DevOps Engineers can benefit from GitLab's built-in Ops tools as part of a single application for the entire software development and operations lifecycle.
  
  
                  [See observability capabilities →](/stages-devops-lifecycle/monitor/){data-ga-name="observability capabilities" data-ga-location="body"}
  
        - name: topics-copy-block
          data:
            column_size: 10
            header: Monitor Kubernetes Environments
            blocks:
              - text: |
                  For cloud native applications, GitLab may be able to completely replace tools like Datadog, New Relic, and Splunk. With Prometheus, Sentry, and Jaeger integrations, GitLab comes with everything you need out of the box for your micro-services based applications. Using GitLab lowers costs by offloading your cloud native monitoring from legacy pay-per-use tools to GitLab's built-in capabilities.
  
  
                  [Prometheus integration →](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#prometheus-integration)
  
        - name: topics-copy-block
          data:
            column_size: 10
            header: Use Auto-generated pipelines
            blocks:
              - text: |
                  Get started quickly with GitLab's Auto DevOps capabilities. With a few clicks, Auto DevOps configures an entire CI/CD pipeline that detects your project's attributes and runs a pipeline based on best practices learned from more than 100K businesses using GitLab. Auto DevOps is fully configurable when you are ready to customize your pipelines to meet your exact specifications.
  
  
                  [Auto DevOps →](/stages-devops-lifecycle/auto-devops/){data-ga-name="auto devops" data-ga-location="body"}
  
  
        - name: topics-copy-block
          data:
            column-size: 10
            header: View all pipelines at a glance
            blocks:
              - text: |
                  Know which projects are green and which are red with a single view. The [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/) provides a summary of each project's operational health, including pipeline and alert status so you can quickly diagnose system-wide problems or drill down into the specific commit causing a failure.
  
  
                  [Operations Dashboard →](https://docs.gitlab.com/ee/user/operations_dashboard/)
  
        - name: topics-copy-block
          data:
            column-size: 10
            header: Incident Management
            blocks:
              - text: |
                    Get up and running faster with a complete DevOps platform featuring incident management built-in. Your Agile project planning, source code, pipelines, and cloud native monitoring all live in GitLab. Imagine having all of this data at your fingertips seamlessly available from your incident management application.
  
  
                    [Incident management →](/handbook/engineering/infrastructure/incident-management/){data-ga-name="incident management" data-ga-location="body"}
  
        - name: topics-copy-block
          data:
            column-size: 10
            header: Serverless
            blocks:
              - text: |
                  Run your own Functions-as-a-Service (FaaS) using GitLab Serverless. Functions and even container-based applications can be easily deployed that auto-scale up to meet demand then down to zero when there's no usage.
  
  
                  [GitLab Serverless →](/topics/serverless/){data-ga-name="serverless" data-ga-location="body"}
        
  
---
  title: GitLab pour les start-up
  description: L'application à guichet unique pour booster votre start-up. Nous offrons nos éditions supérieures gratuitement aux start-up qui répondent aux critères d'éligibilité. En savoir plus !
  image_alt: GitLab pour les start-up
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  GitLab pour les start-up
        title: Accélérez votre croissance
        subtitle: Livrez les releases des logiciels plus rapidement tout en renforçant la sécurité grâce à la plateforme DevSecOps.
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        mobile_title_variant: 'head1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
          text: Commencer votre essai gratuit
          data_ga_name: Start your free trial
          data_ga_location: header
        secondary_btn:
          url: /solutions/startups/join/
          text: Participer au programme GitLab pour les start-up
          data_ga_name: startup join
          data_ga_location: header
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/startups_hero.jpeg
          alt: "Image\_: gitlab pour open source"
          bordered: true
    - name: customer-logos
      data:
        text_variant: body2
        companies:
        - image_url: "/nuxt-images/logos/chorus-color.svg"
          link_label: Link to Chorus customer case study
          alt: "Logo Chorus"
          url: /customers/chorus/
        - image_url: "/nuxt-images/logos/zoopla-logo.png"
          link_label: Link to zoopla customer case study
          alt: "zoopla logo"
          url: /customers/zoopla/
        - image_url: "/nuxt-images/logos/zebra.svg"
          link_label: Link to the zebra customer case study
          alt: "the zebra logo"
          url: /customers/thezebra/
        - image_url: "/nuxt-images/logos/hackerone-logo.png"
          link_label: Link to hackerone customer case study
          alt: "hackerone logo"
          url: /customers/hackerone/
        - image_url: "/nuxt-images/logos/weave_logo.svg"
          link_label: Link to weave customer case study
          alt: "weave logo"
          url: /customers/weave/
        - image_url: /nuxt-images/logos/inventx.png
          alt: "inventx logo"
          url: /customers/inventx/

    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Avantages
          href: '#benefits'
        - title: Clients
          href: '#customers'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:

            - name: 'solutions-video-feature'
              data:
                header: Vitesse. Efficacité. Contrôle
                description: Pour les start-up, déployer des logiciels plus rapidement et plus efficacement n'est pas un luxe. C'est ce qui vous permet de gagner de nouveaux clients, de réaliser vos objectifs en termes de chiffre d'affaires et de différencier vos produits et services de ceux déjà disponibles sur le marché - ou pas. GitLab est la plateforme DevSecOps conçue pour accélérer votre croissance.
                video:
                  url: 'https://player.vimeo.com/video/784035583?h=d978ef89bf&color=7759C2&title=0&byline=0&portrait=0'

        - name: 'div'
          id: benefits
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Pourquoi les start-up choisissent GitLab
                light_background: false
                large_card_on_bottom: true
                cards:
                  - title: Une seule plateforme pour l'ensemble du cycle de vie
                    description: En utilisant une seule application pour l'ensemble du cycle de livraison des logiciels, vos équipes peuvent se concentrer sur la livraison de logiciels de qualité - et non sur la maintenance de nombreuses intégrations de chaînes d'outils. Vous améliorerez la communication, la visibilité globale, la sécurité, la durée des cycles, l'intégration, et bien plus encore.
                    href: /platform/?stage=plan
                    data_ga_name: platform plan
                    icon:
                      name: cycle
                      alt: cycle Icon
                      variant: marketing
                  - title: Déployer vers n'importe quel environnement ou cloud
                    description: "GitLab peut être déployé et géré sur n’importe quel cloud, vous avez donc la liberté de l'utiliser comme vous le souhaitez et où vous le souhaitez, ce qui vous offre la flexibilité de changer de fournisseurs de services cloud ou d'en ajouter de nouveaux au fur et à mesure que votre entreprise se développe. Déployez en toute transparence sur le Cloud de votre choix : AWS, Google Cloud, Azure pour n'en citer que quelques-uns."
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: Sécurité intégrée
                    description: Avec GitLab, la sécurité et la conformité sont intégrées à chaque étape du cycle de la livraison logicielle, ce qui permet à votre équipe d'identifier les vulnérabilités en amont et de rendre le développement plus rapide et plus efficace. À mesure que votre start-up se développe, vous pourrez gérer les risques sans sacrifier la vitesse.
                    href: /solutions/security-compliance/
                    data_ga_name: solutions dev-sec-ops
                    icon:
                      name: devsecops
                      alt: devsecops Icon
                      variant: marketing
                  - title: Créer et déployer des logiciels plus rapidement
                    description: GitLab vous permet de réduire les temps de cycle et de déployer plus fréquemment en ménageant vos efforts. Vous pouvez tout faire, de la planification de nouvelles fonctionnalités aux tests automatisés, en passant par l'orchestration des versions, le tout dans une seule application.
                    href: /platform/
                    data_ga_name: platform
                    icon:
                      name: speed-gauge
                      alt: speed-gauge Icon
                      variant: marketing
                  - title: Collaboration renforcée
                    description: Brisez les silos et donnez à l'ensemble de votre personnel les moyens de collaborer autour de votre code qu'il s'agisse des équipes de développement, de sécurité et d'opérations ou des équipes commerciales, en passant par les parties prenantes non techniques. Grâce à la plateforme DevSecOps, vous pouvez accroître la visibilité sur l'ensemble du cycle de vie et améliorer la communication pour toutes les personnes qui collaborent pour faire avancer les projets logiciels.
                    icon:
                      name: collaboration-alt-4
                      alt: Icône de collaboration
                      variant: marketing
                  - title: Plateforme open source
                    description: L'approche commerciale à noyau ouvert de GitLab vous offre tous les avantages des logiciels open source, notamment la possibilité de tirer parti des innovations de milliers de développeurs du monde entier qui travaillent en permanence à leur amélioration et à leur perfectionnement.
                    icon:
                      name: open-source
                      alt: open-source Icon
                      variant: marketing
                  - title: GitLab évolue avec vous
                    description: Du capital pour démarrer votre entreprise au flux de trésorerie d'exploitation positif, en passant par l'évolution vers une grande entreprise multinationale (ou quel que soit votre objectif), GitLab grandira à vos côtés.
                    href: /customers/
                    data_ga_name: customers
                    cta: Afficher certains des clients de GitLab
                    icon:
                      name: auto-scale
                      alt: auto-scale Icon
                      variant: marketing
        - name: 'div'
          id: customers
          slot_enabled: true
          slot_content:
            - name: 'education-case-study-carousel'
              data:
                header: Découvrez comment nous avons aidé des start-up comme la vôtre
                customers_cta: true
                cta:
                  href: '/customers/'
                  text: Afficher d'autres témoignages
                  data_ga_name: customers
                case_studies:
                  - logo_url: "/nuxt-images/logos/chorus-color.svg"
                    institution_name: Chorus
                    quote:
                      img_url: /nuxt-images/blogimages/Chorus_case_study.png
                      quote_text: La vie est tellement plus facile pour l'ingénierie produit et toutes les personnes qui veulent interagir avec ce domaine, car elles peuvent simplement le faire en utilisant à GitLab.
                      author: Russell Levy
                      author_title: Cofondateur et directeur technique, Chorus.ai
                    case_study_url: /customers/chorus/
                    data_ga_name: chorus case study
                    data_ga_location: case study carousel
                  - logo_url:  "/nuxt-images/logos/hackerone-logo.png"
                    institution_name: hackerone
                    quote:
                      img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
                      quote_text: GitLab nous aide à détecter rapidement les failles de sécurité et la plateforme les intègre dans le flux de travail des développeurs. Les développeurs peuvent pousser du code dans GitLab CI et immédiatement recevoir des commentaires lors de l'une des nombreuses étapes d'audit en cascade. Ils peuvent ainsi constater la présence éventuelle d'une vulnérabilité de sécurité. De plus, les développeurs peuvent même créer de nouvelles étapes qu'ils jugent nécessaires pour tester des problèmes de sécurité très spécifiques.
                      author: Mitch Trale
                      author_title: Directeur de l'infrastructure, HackerOne
                    case_study_url: /customers/hackerone/
                    data_ga_name: hackerone case study
                    data_ga_location: case study carousel
                  - logo_url: /nuxt-images/logos/anchormen-logo.svg
                    institution_name: Anchormen
                    quote:
                      img_url: /nuxt-images/blogimages/anchormen.jpg
                      quote_text: Vous pouvez vraiment vous concentrer sur votre travail. Vous n'avez pas besoin de penser à toutes ces autres choses, car une fois que vous avez installé GitLab, vous disposez en quelque sorte d'un filet de sécurité. GitLab vous protège, et vous pouvez vraiment vous concentrer sur la logique métier de l'entreprise. Je dirais sans aucun doute que GitLab améliore l'efficacité opérationnelle.
                      author: Jeroen Vlek
                      author_title: Directeur technique,  Anchormen
                    case_study_url: /customers/anchormen/
                    data_ga_name: anchormen case study
                    data_ga_location: case study carousel


    - name: cta-block
      data:
        cards:
          - header: Déterminez si le programme GitLab pour les start-up est fait pour vous.
            icon: increase
            link:
              text: Programme GitLab pour les start-up
              url: /solutions/startups/
              data_ga_name: startup join
          - header: Combien vous coûte votre chaîne d'outils actuelle ?
            icon: piggy-bank-alt
            link:
              text: Calculer votre retour sur investissement
              url: /calculator/roi/
              data_ga_name: roi calculator

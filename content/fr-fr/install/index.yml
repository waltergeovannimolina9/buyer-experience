---
  title: Télécharger et installer GitLab
  description: Téléchargez, installez et maintenez votre propre instance GitLab avec divers paquets d'installation et téléchargements pour Linux, Kubernetes, Docker, Google Cloud et plus encore.
  side_menu:
    anchors:
      text: "Rubriques"
      data:
      - text: Paquet Linux officiel
        href: "#official-linux-package"
        data_ga_name: official linux package
        data_ga_location: side menu
      - text: Déploiements Kubernetes
        href: "#kubernetes-deployments"
        data_ga_name: kubernetes deployments
        data_ga_location: side menu
      - text: Cloud pris en charge
        href: "#supported-cloud"
        data_ga_name: supported cloud
        data_ga_location: side menu
      - text: Autres méthodes officielles
        href: "#other-official-methods"
        data_ga_name: other official methods
        data_ga_location: side menu
      - text: Contribution de la communauté
        href: "#community-contributed"
        data_ga_name: community contributed
        data_ga_location: side menu
      - text: Déjà installé ?
        href: "#already-installed"
        data_ga_name: already installed
        data_ga_location: side menu
    hyperlinks:
      text: "Plus sur ce sujet"
      data:
        - text: "Essayez GitLab Ultimate gratuitement"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "side-navigation"
        - text: "Installez GitLab auto-géré"
          href: "/free-trial/"
          data_ga_name: "self managed trial"
          data_ga_location: "side-navigation"
        - text: "Démarrez avec GitLab SaaS"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "saas get started"
          data_ga_location: "side-navigation"
        - text: "Achetez sur les plateformes en ligne partenaires"
          href: "https://page.gitlab.com/cloud-partner-marketplaces.html"
          data_ga_name: "partner marketplace"
          data_ga_location: "side-navigation"
  header:
    title: Installer GitLab auto-géré
    subtitle: Essayez GitLab dès aujourd'hui. Téléchargez, installez et maintenez votre propre instance de GitLab.
    text: |
      [Essai](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="free trial" data-ga-location="header"} – essayez GitLab Ultimate gratuitement dès aujourd'hui

      [GitLab auto-géré](/free-trial/){data-ga-name="self managed trial" data-ga-location="header"} – installez cette version sur votre propre infrastructure

      [GitLab SaaS](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="saas get started" data-ga-location="header"} – démarrez avec notre offre SaaS

      [Achat en ligne](https://page.gitlab.com/cloud-partner-marketplaces.html){data-ga-name="partner marketplace" data-ga-location="header"} - achetez GitLab en toute transparence sur la plateforme d'achat Cloud de votre choix
  linux:
    title: Paquet Linux officiel
    subtitle: Méthode d'installation recommandée
    text: |
      Voici la méthode d'installation recommandée pour démarrer avec le paquet Linux. Les paquets Linux sont matures, évolutifs et sont utilisés aujourd'hui sur GitLab.com. Si vous avez besoin de davantage de flexibilité et de résilience, nous vous recommandons de déployer GitLab comme décrit dans la [documentation de l'architecture de référence.](https://docs.gitlab.com/ee/administration/reference_architectures/index.html){data-ga-name="reference architecture documentation" data-ga-location="linux installation"}

      L'installation Linux est plus rapide, plus facile à mettre à jour et contient des caractéristiques qui améliorent la fiabilité et que l'on ne retrouve pas dans les autres méthodes. L'installation se fait à l'aide d'un paquet unique (également connu sous le nom d'Omnibus) qui regroupe tous les différents services et outils requis pour faire fonctionner GitLab. Il est recommandé de disposer d'au moins 4 Go de RAM ([exigences minimales](https://docs.gitlab.com/ee/install/requirements.html){data-ga-name="installation requirements documentation" data-ga-location="linux installation"}).

      Veuillez consulter notre dépôt de paquets ([GitLab-ee](https://packages.gitlab.com/gitlab/gitlab-ee) or [GitLab-ce](https://packages.gitlab.com/gitlab/gitlab-ce)) pour vous assurer que la version de GitLab requise est disponible pour la version du système d'exploitation hôte.
    cards:
      - title: Ubuntu
        id: ubuntu
        subtext: 18.04 LTS, 20.04 LTS, 22.04 LTS
        icon:
          name: ubuntu-purple
          alt: Icône Ubuntu
        link_text: Consulter les instructions d'installation +
        link_url: /install/#ubuntu
        data_ga_name: ubuntu installation documentation
        data_ga_location: linux installation
      - title: Debian
        id: debian
        subtext: 10, 11
        icon:
          name: debian
          alt: Icône Debian
        link_text: Consulter les instructions d'installation +
        link_url: /install/#debian
        data_ga_name: debian installation documentation
        data_ga_location: linux installation
      - title: AlmaLinux
        id: almalinux
        subtext: versions 8, 9 pour les distributions compatibles RHEL
        icon:
          name: almalinux
          variant: marketing
          alt: Almalinux Icon
        link_text: Voir les instructions d'installation +
        link_url: /fr-fr/install/#almalinux
        data_ga_name: almalinux installation documentation
        data_ga_location: linux installation
      - title: CentOS 7
        id: centos-7
        subtext: et RHEL, Oracle, Scientific
        icon:
          name: centos
          alt: Icône CentOs
        link_text: Consulter les instructions d'installation +
        link_url: /install/#centos-7
        data_ga_name: centos 7 installation documentation
        data_ga_location: linux installation
      - title: OpenSUSE Leap
        id: opensuse-leap
        subtext: OpenSUSE Leap 15.4 et SUSE Linux Enterprise Server 12.2, 12.5
        icon:
          name: opensuse
          alt: Icône OpenSuse
        link_text: Consulter les instructions d'installation +
        link_url: /install/#opensuse-leap
        data_ga_name: opensuse leap installation documentation
        data_ga_location: installation linux
      - title: Amazon Linux 2
        id: amazonlinux-2
        subtext:
        icon:
          name: aws
          alt: Icône AWS
        link_text: Consulter les instructions d'installation +
        link_url: /install/#amazonlinux-2
        data_ga_name: amazonlinux 2 installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2022
        id: amazonlinux-2022
        subtext:
        icon:
          name: aws
          alt: Icône AWS
        link_text: Consulter les instructions d'installation +
        link_url: /install/#amazonlinux-2022
        data_ga_name: amazonlinux 2022 installation documentation
        data_ga_location: linux installation

      - title: Système d'exploitation Raspberry Pi
        id: raspberry-pi-os
        subtext: Bullseye et Buster (32 bits)
        icon:
          name: raspberry-pi
          alt: Icône Raspberry Pi
        link_text: Consulter les instructions d'installation +
        link_url: /install/#raspberry-pi-os
        data_ga_name: raspberry pi os installation documentation
        data_ga_location: linux installation
    dropdowns:
      - id: ubuntu
        tip: |
          Pour Ubuntu 20.04 et 22.04, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        first_step: 1. Installez et configurez les dépendances nécessaires
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
          ```
        postfix_command: |
          ```
           sudo apt-get install -y postfix
          ```
        download_command: |
          ```
           curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
           sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        link_back: ubuntu
      - id: debian
        tip: |
          Pour Debian 10, les paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates perl
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
            curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        line_back: debian
      - id: almalinux
        tip: |
          Pour AlmaLinux et RedHat versions 8 et 9, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur AlmaLinux (et RedHat) versions 8 et 9, les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez la sauter si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: almalinux
      - id: centos-7
        dependency_text: Pour CentOS 7 (et RedHat/Oracle/Linux Scientific 7), les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez l'ignorer si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: centos-7
      - id: opensuse-leap
        tip: |
          Pour OpenSuse, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur OpenSUSE, les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez l'ignorer si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo zypper install curl openssh perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo zypper install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash```
        install_command: |
          ```sudo EXTERNAL_URL="https://gitlab.example.com" zypper install gitlab-ee```
        line_back: opensuse-leap

      - id: amazonlinux-2022
        tip: |
          Pour Amazon Linux 2022, des paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur Amazon Linux 2022, les commandes ci-dessous ouvriront également l'accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez l'ignorer si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: amazonlinux-2022

      - id: amazonlinux-2
        tip: |
          Pour Amazon Linux 2, les paquets `arm64` sont également disponibles et seront automatiquement utilisés sur cette plateforme lors de l'utilisation du dépôt GitLab pour l'installation.
        dependency_text: Sur Amazon Linux 2, les commandes ci-dessous ouvriront également les accès HTTP, HTTPS et SSH dans le pare-feu du système. Cette étape est facultative, et vous pouvez l'ignorer si vous avez l'intention d'accéder à GitLab uniquement à partir de votre réseau local.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server openssh-clients perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: amazonlinux-2
      - id: raspberry-pi-os
        tip: |
          Il est recommandé de disposer d'au moins 4 Go avec Raspberry Pi 4. Seul la version 32 bits (armhf) est supportée pour le moment. La version 64 bits (`arm64`) est en cours de développement.
        dependency_command: |
          ```
          sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
          curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
          sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
          ```
  kubernetes:
    title: Déploiements Kubernetes
    text: |
      Lorsque vous installez GitLab sur Kubernetes, vous devez tenir compte des quelques compromis suivants :

        - L'administration et le dépannage nécessitent des connaissances sur Kubernetes.
        - Cela peut être plus coûteux pour les petites installations. L'installation par défaut nécessite plus de ressources qu'un déploiement d'un paquet Linux à un seul nœud, car la plupart des services sont déployés de manière redondante.
        - Vous devez [tenir compte de certaines limitations](https://docs.gitlab.com/charts/#limitations){data-ga-name="chart limitations" data-ga-location="kubernetes installation"} concernant les fonctionnalités.

      Utilisez cette méthode si votre infrastructure est construite sur Kubernetes et que vous êtes familier avec son fonctionnement. Les méthodes de gestion, d'observabilité et certains concepts sont différents des déploiements traditionnels. La méthode du graphique Helm concerne les déploiements Vanilla Kubernetes, et l'opérateur GitLab peut être utilisé pour déployer GitLab sur un cluster OpenShift. L'opérateur GitLab peut être utilisé pour automatiser les opérations du jour 2 dans les déploiements sur un cluster OpenShift et un cluster Vanilla Kubernetes.
    cards:
      - title: Graphique Helm
        subtext: Installer GitLab à l'aide des graphiques Helm
        icon:
          name: kubernetes-purple
          alt: Icône Kubernetes
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/charts/
        data_ga_name: helm charts
        data_ga_location: kubernetes installation
      - title: Opérateur GitLab
        new_flag: true
        subtext: Installer GitLab à l'aide de l'opérateur
        icon:
          name: gitlab-operator
          alt: Icône de l'opérateur GitLab
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/charts/installation/operator.html
        data_ga_name: gitlab operator
        data_ga_location: kubernetes installation
  supported_cloud:
    title: Cloud pris en charge
    text: |
      Utilisez le paquet Linux officiel pour installer GitLab sur différents fournisseurs de services en Cloud.
    cards:
      - title: Amazon Web Services (AWS)
        subtext: Installer GitLab sur AWS
        icon:
          name: aws
          alt: Icône AWS
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/aws/
        data_ga_name: aws install documentation
        data_ga_location: supported cloud
      - title: Google Cloud Platform (GCP)
        subtext: Installer GitLab sur GCP
        icon:
          name: gcp
          alt: Icône GCP
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/google_cloud_platform/
        data_ga_name: gcp install documentation
        data_ga_location: supported cloud
      - title: Microsoft Azure
        subtext: Installer GitLab sur Azure
        icon:
          name: azure
          alt: Icône Azure
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/azure/
        data_ga_name: azure install documentation
        data_ga_location: supported cloud
  official_methods:
    title: Autres méthodes d'installation officielles prises en charge
    cards:
      - title: Docker
        subtext: Images Docker officielles de GitLab
        icon:
          name: docker
          alt: Icône Docker
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/docker.html
        data_ga_name: docker install documentation
        data_ga_location: official installation
      - title: Architectures de référence
        subtext: Topologies de déploiement recommandées pour GitLab
        icon:
          name: gitlab-tanuki
          alt: Icône GitLab
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
        data_ga_name: reference architectures install documentation
        data_ga_location: official installation
      - title: Installation à partir de la source
        subtext: Installer GitLab à l'aide des fichiers sources sur un système Debian/Ubuntu
        icon:
          name: source
          alt: Source Icon
          variant: marketing
          hex_color: '#336CE4'
        link_text: Consulter les instructions d'installation
        link_url: https://docs.gitlab.com/ee/install/installation.html
        data_ga_name: installation from source
        data_ga_location: official installation
      - title: Boîte à outils de l'environnement GitLab (GET)
        subtext: Automatisation du déploiement des architectures de référence GitLab à l'aide de Terraform et Ansible
        icon:
          name: gitlab-environment-toolkit
          alt: Icône de la boîte à outils de l'environnement GitLab
          variant: marketing
        link_text: Consulter les instructions d'installation
        link_url: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
        data_ga_name: gitlab environment toolkit installation
        data_ga_location: official installation
  unofficial_methods:
    title: Méthodes d'installation non officielles et non prises en charge
    cards:
      - title: Paquet natif Debian
        subtext: de Pirate Praveen
        icon:
          name: debian
          alt: Icône Debian
        link_text: Consulter les instructions d'installation
        link_url: https://wiki.debian.org/gitlab/
        data_ga_name: debian native installation
        data_ga_location: unofficial installation
      - title: Paquet FreeBSD
        subtext: de Torsten Zühlsdorff
        icon:
          name: freebsd
          alt: Icône FreeBSD
        link_text: Consulter les instructions d'installation
        link_url: http://www.freshports.org/www/gitlab-ce
        data_ga_name: freebsd installation
        data_ga_location: unofficial installation
      - title: Paquet Arch Linux
        subtext: de la communité Arch Linux
        icon:
          name: arch-linux
          alt: Icône Arch Linux
        link_text: Consulter les instructions d'installation
        link_url: https://archlinux.org/packages/extra/x86_64/gitlab/
        data_ga_name: arch linux installation
        data_ga_location: unofficial installation
      - title: Module Puppet
        subtext: de Vox Pupuli
        img_src: /nuxt-images/install/puppet-logo.svg
        icon:
          name: puppet
          alt: Icône Puppet
        link_text: Consulter les instructions d'installation
        link_url: https://forge.puppet.com/puppet/gitlab
        data_ga_name: puppet module installation
        data_ga_location: unofficial installation
      - title: Playbook Ansible
        subtext: de Jeff Geerling
        icon:
          name: ansible-purple
          alt: Icône Ansible
        link_text: Consulter les instructions d'installation
        link_url: https://github.com/geerlingguy/ansible-role-gitlab
        data_ga_name: ansible installation
        data_ga_location: unofficial installation
      - title: Machine virtuelle GitLab (KVM)
        subtext: de OpenNebula
        icon:
          name: open-nebula
          alt: Icône OpenNebula
        link_text: Consulter les instructions d'installation
        link_url: https://marketplace.opennebula.io/appliance/6b54a412-03a5-11e9-8652-f0def1753696
        data_ga_name: opennebula installation
        data_ga_location: unofficial installation
      - title: GitLab sur Cloudron
        subtext: via la bibliothèque d'applications Cloudron
        img_src: /nuxt-images/install/cloudron-logo.svg
        icon:
          name: cloudron
          alt: Icône Cloudron
        link_text: Consulter les instructions d'installation
        link_url: https://cloudron.io/store/com.gitlab.cloudronapp.html
        data_ga_name: cloudron installation
        data_ga_location: unofficial installation
  updates:
    title: Vous avez déjà installé GitLab ?
    cards:
      - title: Mise à jour d'une ancienne version de GitLab
        text: Mettez à jour votre installation de GitLab pour bénéficier des dernières fonctionnalités. Les versions de GitLab qui incluent de nouvelles fonctionnalités sont publiées le 22 de chaque mois.
        link_url: https://about.gitlab.com/update/
        link_text: Update to the latest release of GitLab
        data_ga_name: Update to the latest relase of GitLab
        data_ga_location: update
      - title: Mise à jour de la version GitLab Community Edition
        text: GitLab Enterprise Edition comprend des caractéristiques et des fonctionnalités avancées qui ne sont pas disponibles dans la version Community Edition.
        link_url: https://about.gitlab.com/upgrade/
        link_text: Upgrade to Enterprise Edition
        data_ga_name: Upgrade to Enterprise Edition
        data_ga_location: update
      - title: Mise à niveau d'un paquet Omnibus installé manuellement
        text: Avec GitLab 7.10, nous avons introduit des dépôts de paquets pour GitLab, qui vous permettent d'installer GitLab à l'aide d'une simple commande.
        link_url: https://about.gitlab.com/upgrade-to-package-repository/
        link_text: Upgrade to Omnibus package repository
        data_ga_name: Upgrade to Omnibus package repository
        data_ga_location: update
      - title: Applications tierces prenant en charge GitLab
        text: GitLab est ouvert à la collaboration et s'engage à établir des partenariats technologiques dans l'écosystème DevOps. En savoir plus sur les avantages et exigences à remplir pour devenir un Partenaire technologique GitLab.
        link_url: https://about.gitlab.com/partners/
        link_text: View third-party applications
        data_ga_name: View third-party applications
        data_ga_location: update

---
  title: Kontakt zum GitLab-Vertrieb
  description: "Wenden Sie sich an das GitLab-Vertriebsteam, wenn Sie eine individuelle Demo sehen möchten, Hilfe bei der Auswahl des richtigen Tarifs benötigen oder Antworten auf häufig gestellte Fragen erhalten möchten."
  components:
    - name: crop-infinity-hero
      data:
        title: Sprechen Sie mit einem Experten
        subtitle: Möchten Sie eine individuelle Demo sehen oder benötigen Sie Hilfe bei der Auswahl des richtigen Tarifs? Wir freuen uns darauf, Sie kennenzulernen.
    - name: sales-form
      data:
        form:
          form_id: 2922
          form_header: ''
          datalayer: sales
          form_required_text: Füllen Sie alle Felder aus.
        card:
          title: Benötigen Sie technische Hilfe?
          description: Wenden Sie sich bitte an den Kundendienst, indem Sie eine Anfrage auf unserer GitLab-Supportseite übermitteln
          link:
            text: Support kontaktieren
            href: https://support.gitlab.com/
            ga_name: Support
            ga_location: body
        quotes_carousel:
          quotes:
            - title_img:
                url: /nuxt-images/logos/nvidia-logo-horizontal.png
                alt: Nvidia Logo
              quote: |
                Ohne GitLab würden wir unsere Entwicklungszeit mit vielen einzelnen kleinen Servern verschwenden, die weltweit verwaltet werden. Wir hätten wahrscheinlich viel mehr Schwierigkeiten und immer noch mit Skalierungsproblemen zu kämpfen.
              author: Patrick Herlihy, Spezialist für Konfigurationsmanagement, NVIDIA
              url: /customers/nvidia/
              data_ga_name: nvidia
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logo_ticketmaster_2_color.svg
                alt: Ticketmaster Logo
              quote: |
                Bis vor wenigen Monaten sah unsere CI-Pipeline noch ziemlich mager aus. Jetzt ist die Situation eine ganz andere. Wenn Ihr Team nach einer Möglichkeit sucht, einer bestehenden CI-Pipeline neues Leben einzuhauchen, empfehle ich Ihnen, einen Blick auf GitLab CI zu werfen. Für unser mobiles Team bei Ticketmaster war dies ein echter Wendepunkt.
              author: Jeff Kelsey, Leitender Android-Ingenieur, Ticketmaster
              url: /blog/2017/06/07/continuous-integration-ticketmaster/
              data_ga_name: ticketmaster case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logo_goldman_sachs-color.svg
                alt: Goldman Sachs Logo
              quote: |
                GitLab hat es uns ermöglicht, die Geschwindigkeit der Entwicklung in unserer Entwicklungsabteilung drastisch zu erhöhen. Wir glauben, dass das Bestreben von GitLab, Software schnell und effektiv auf den Markt zu bringen, anderen Unternehmen helfen wird, die gleiche Steigerung der Effizienz zu erreichen, die wir bei Goldman Sachs erlebt haben. Einige Teams führen jetzt täglich mehr als 1000 CI-Feature-Branch-Builds aus und führen sie zusammen!
              author: Andrew Knight, Geschäftsführender Direktor, Goldman Sachs
              url: /customers/goldman-sachs/
              data_ga_name: goldman sachs case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/equinix-logo.svg
                alt: Equinix Logo
              quote: |
                Der Umstieg auf ein System wie GitLab hilft jeder Organisation oder jedem Unternehmen dabei, in die DevOps-Methodik einzusteigen und Bereitstellungsabläufe kontinuierlich zu verbessern, um Qualität, Agilität und Selbstverwaltungsfähigkeit zu erreichen.
              author: Bala Kannan, Senior Software Engineer, Equinix
              url: /customers/equinix/
              data_ga_name: equinix case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logo_siemens_color.svg
                alt: Siemens Logo
              quote: |
                GitLab ist ein großartiges Produkt und verfügt über eine der freundlichsten und gesündesten Open-Source-Communities.
              author: Alexis Reigel, Senior Software Engineer, Siemens
              url: /blog/2018/12/18/contributor-post-siemens/
              data_ga_name: siemens case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/usarmy.svg
                alt: US Army Logo
              quote: |
                Statt Mitarbeitern 10 verschiedene Dinge beibringen zu müssen, lernen sie nur eine Sache, was auf lange Sicht viel einfacher ist.
              author: JChris Apsey, Captain, U.S. Army Cyber School
              url: /customers/us_army_cyber_school/
              data_ga_name: us army cyber school case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logoradiofrance.svg
                alt: Radio France Logo
              quote: |
                Unser Hauptziel war es, mehrere Tools in einem einzigen zu vereinen und Entwicklern die Bereitstellung von Produkten in der Produktion zu erleichtern. Vor der Migration waren es zehn pro Tag. Jetzt, mit GitLab, führen wir in der Produktion täglich 50 Bereitstellungen durch. Das ist viel effizienter als zu der Zeit, als wir noch zwischen GitLab und Jenkins wechseln mussten.
              author: Julien Vey, Operational Excellence Manager, Radio France
              url: /customers/radiofrance/
              data_ga_name: radio france case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/hackerone-logo.png
                alt: HackerOne Logo
              quote: |
                GitLab hilft uns dabei, Sicherheitslücken frühzeitig zu erkennen. Es ist inzwischen fest in die Arbeitsabläufe der Entwickler integriert. Entwickler können Code in GitLab CI pushen und eine sofortige Rückmeldung von einem der vielen kaskadierenden Audit-Schritte erhalten. So können sie sofort sehen, ob es eine Sicherheitslücke gibt. Zusätzlich können Entwickler eigene neue Schritte erstellen, die ganz bestimmte Sicherheitsprobleme testen.
              author: Mitch Trale, Leiter Infrastruktur, HackerOne
              url: /customers/hackerone/
              data_ga_name: hacker one case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/worldline-logo.svg
                alt: Worldline Logo
              quote: |
                GitLab ist das Rückgrat unserer Entwicklungsumgebung. Mittlerweile arbeiten täglich 2.500 Menschen für uns damit.
              author: Antoine Neveux, Softwaretechnik – Team Kazan, Worldline
              url: /customers/worldline/
              data_ga_name: worldline case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/uw-logo.svg
                alt: University of Washington Logo
              quote: |
                In den letzten zwei Jahren hat GitLab unsere Organisation hier an der UW stark verändert. Ihre Plattform ist fantastisch!
              author: Aaron Timss, Direktor für Informationstechnologie, CSE
              url: /customers/uw/
              data_ga_name: university of washington case study
              data_ga_location: body
  faq:
    header: Häufig gestellte Fragen (FAQ)
    groups:
      - header: "Lizenz und Abonnement"
        data_toggle: License and Subscription
        questions:
          - question: Ich habe bereits ein Konto, wie kann ich ein Upgrade durchführen?
            answer: >-
              Gehen Sie zu [https://customers.gitlab.com](https://customers.gitlab.com){data-ga-name="customers" data-ga-location="body"} und wählen Sie ein neues Paket aus.
          - question: Kann ich meinem Abonnement weitere Benutzer(innen) hinzufügen?
            answer: >-
              Ja. Sie haben mehrere Optionen. Sie können Ihrem Abonnement jederzeit während der Laufzeit des Abonnements Benutzer(innen) hinzufügen. Sie können sich über das [GitLab-Kundenportal](https://customers.gitlab.com){data-ga-name="gitlab customers portal" data-ga-location="body"} an Ihrem Konto anmelden und weitere Plätze hinzufügen oder für ein Angebot das [Vertriebsteam kontatieren](/sales/){data-ga-name="contact sales" data-ga-location="body"}. In beiden Fällen werden die Kosten anteilig ab dem Datum des Angebots/Kaufs bis zum Ende des Abonnementzeitraums berechnet. Sie können für die zusätzlichen Lizenzen auch im Rahmen unseres True-up-Modells bezahlen.
          - question: Wie werden zusätzliche Benutzer abgerechnet?
            answer: |
              Wenn Sie den [vierteljährlichen Abonnementabgleich](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html){ data-ga-name="reconciliation" data-ga-location="body"} aktiviert haben (Standardoption für neue und verlängerte Abonnements nach dem 1. August 2021), werden Benutzer(innen), die während eines Quartals hinzugefügt werden, nur für die verbleibenden Quartale der Abonnementlaufzeit in Rechnung gestellt und nicht für den vollen jährlichen Abonnementspreis mit jährlichen True-ups. Wenn Sie beispielsweise im dritten Quartal Ihrer Abonnementlaufzeit 50 Benutzer zu Ihrem Abonnement hinzufügen, werden die 50 Benutzer nur für das vierte Quartal Ihrer Abonnementlaufzeit berechnet und nicht für alle vier Quartale, wie bei den jährlichen True-ups.

              Wenn Sie den vierteljährlichen Abonnementabgleich nicht aktiviert haben, werden zusätzliche Benutzer für die gesamte Laufzeit, in der sie hinzugefügt wurden, mit einem jährlichen True-up abgerechnet. Wenn Sie zum Beispiel heute 100 aktive Benutzer haben, sollten Sie ein Abonnement für 100 Benutzer erwerben. Nehmen wir an, dass Sie bei der Erneuerung im nächsten Jahr 300 aktive Benutzer haben (200 zusätzliche Benutzer). Wenn Sie dann Ihr Abonnement verlängern, zahlen Sie für 300 Benutzer und für die 200 Benutzer, die Sie im Laufe des vergangenen Jahres hinzugefügt haben, ebenfalls die volle Jahresgebühr.
          - question: Was passiert, wenn mein Abonnement bald abläuft oder abgelaufen ist?
            answer: >-
              Sie erhalten eine neue Lizenz, die Sie in Ihre GitLab-Instanz hochladen müssen. Folgen Sie dafür [dieser Anleitung](https://docs.gitlab.com/ee/user/admin_area/license.html){ data-ga-name="licence" data-ga-location="body"}.
          - question: Was passiert, wenn ich mich entscheide, mein Abonnement nicht zu verlängern?
            answer: >-
              14 Tage nach Ablauf Ihres Abonnements wird Ihr Schlüssel nicht mehr funktionieren und die GitLab Enterprise Edition ist nicht mehr nutzbar. Sie können ein Downgrade auf die GitLab Community Edition durchführen, die Sie kostenlos nutzen können.
          - question: Kann ich eine Kombination von Lizenzen erwerben?
            answer: >-
              Nein, alle Benutzer(innen) in der Gruppe müssen den gleichen Tarif nutzen.
          - question: Wie funktioniert der Lizenzschlüssel?
            answer: >-
              Der Lizenzschlüssel ist eine statische Datei, die nach dem Hochladen die Ausführung der GitLab Enterprise Edition ermöglicht. Während des Lizenz-Uploads überprüfen wir, ob die Anzahl der aktiven Benutzer auf Ihrer GitLab Enterprise Edition-Instanz die neue Anzahl der Benutzer nicht übersteigt. Während des Lizenzzeitraums können Sie so viele Benutzer hinzufügen, wie Sie möchten. Für GitLab-Abonnenten läuft der Lizenzschlüssel nach einem Jahr ab.

      - header: "Zahlungen und Preise"
        data_toggle: Payments and Pricing
        questions:
          - question: Was ist ein Benutzer?
            answer: >-
              Als Benutzer gilt jeder einzelne Endbenutzer (Person oder Maschine) des Kunden und/oder seiner Vertragspartner (einschließlich, aber nicht beschränkt auf deren Team-Mitglieder, Vertreter und Berater), der Zugang zu den lizenzierten Materialien gemäß diesem Vertrag hat.
          - question: Handelt es sich bei den angegebenen Preisen um Pauschalpreise?
            answer: >-
              Die aufgeführten Preise können lokalen Steuern und Quellensteuern unterliegen. Beim Kauf über einen Partner oder Wiederverkäufer können die Preise variieren.
          - question: Welche Funktionen sind in GitLab Self-Managed und SaaS in den verschiedenen Preisplänen enthalten?
            answer: >-
              Eine aktuelle Liste finden Sie auf der [Featureseite](/features/){ data-ga-name="features page" data-ga-location="body"}.
          - question: Kann ich meine Projekte von einem anderen Anbieter importieren?
            answer: >-
              Ja. Sie können Ihre Projekte von den meisten bestehenden Anbietern importieren, einschließlich GitHub und Bitbucket. Weitere Informationen zu allen Import-Optionen finden Sie in unserer [Dokumentation](https://docs.gitlab.com/ee/user/project/import/index.html){ data-ga-name="see our documentation" data-ga-location="body"}.
          - question: Gibt es Sonderpreise für Open-Source-Projekte, Bildungseinrichtungen oder Start-ups?
            answer: >-
              Ja! Wir stellen qualifizierten Open-Source-Projekten, Bildungseinrichtungen und Start-ups kostenlose Ultimate-Lizenzen sowie 50.000 Compute-Einheiten/Monat zur Verfügung. Weitere Informationen dazu finden Sie auf unseren Programmseiten [GitLab für Open Source](/solutions/open-source/){ data-ga-name="open source" data-ga-location="body"}, [GitLab für Bildungseinrichtungen](/solutions/education/){ data-ga-name="education" data-ga-location="body"} und [GitLab für Start-ups](/solutions/startups/){ data-ga-name="startups" data-ga-location="body"}.
          - question: Wie legt GitLab fest, welche zukünftigen Funktionen in welchen Tarifen verfügbar sein werden?
            answer: >-
              Auf dieser Seite stellen wir unsere [Leistungen](/company/pricing/#capabilities){ data-ga-name="capabilities" data-ga-location="body"} vor. Diese sind als Filter für unser [käuferbasiertes Open-Core-Preismodell](/company/pricing/#buyer-based-tiering-clarification){ data-ga-name="open core" data-ga-location="body"} gedacht. Auf unserer [Handbuchseite zur Preisgestaltung](/handbook/ceo/pricing){ data-ga-name="pricing decisions" data-ga-location="body"} erfahren Sie mehr darüber, wie wir unsere Entscheidungen zur Staffelung der Tarife treffen.

      - header: Funktionen und Vorteile
        data_toggle: Features and Benefits
        questions:
          - question: Was sind die Unterschiede zwischen den Paketen Free, Premium und Ultimate?
            answer: >-
              Alle Funktionen und Vorteile der verschiedenen GitLab-Angebote finden Sie auf den Seiten zum [Funktionsvergleich](/features/){ data-ga-name="feature comparison pages" data-ga-location="body"}. Erfahren Sie mehr darüber, ob die Pakete [Premium](/pricing/premium){ data-ga-name="premium" data-ga-location="body"} und [Ultimate](/pricing/ultimate){ data-ga-name="ultimate" data-ga-location="body"} die richtigen Tarife für Sie sind.
          - question: Was ist der Unterschied zwischen GitLab und anderen DevOps-Lösungen?
            answer: >-
              Alle Unterschiede zwischen GitLab und anderen beliebten DevOps-Lösungen finden Sie auf unseren Seiten zum [Vergleich der verschiedenen Anbieter](/devops-tools/){ data-ga-name="devops tools" data-ga-location="body"}.

      - header: "GitLab SaaS"
        data_toggle: GitLab SaaS
        questions:
          - question: Wo wird SaaS gehostet?
            answer: >-
              Derzeit hosten wir auf der Google Cloud Plattform in den USA.
          - question: Welche Funktionen sind für GitLab SaaS nicht verfügbar?
            answer: >-
              Einige Funktionen sind nur für GitLab Self-Managed verfügbar und nicht für SaaS. Eine aktuelle Liste finden Sie auf der [Features-Seite](/features/){data-ga-name="features page saas" data-ga-location="body"}.

      - header: "Speicher- und Übertragungsbegrenzungen"
        data_toggle: Storage and Transfer Limits
        questions:
          - question: Gelten die Speicher- und Übertragungsbegrenzungen auch für GitLab Self-Managed?
            answer: >-
              Nein. Für selbstverwaltete Instanzen gibt es keine Grenzwerte für den Speicher- und Übertragungsumfang. Die Administrator(inn)en sind für die zugrunde liegenden Infrastrukturkosten verantwortlich.
          - question: Was passiert, wenn ich meine Speicher- und Übertragungsbegrenzungen überschreite?
            answer: >-
              Bei den Grenzwerten handelt es sich derzeit nur um weiche Grenzwerte. GitLab wird diese Grenzwerte nicht automatisch durchsetzen, da das Produkt noch nicht die Nutzung aller Speicher- und Übertragungsmöglichkeiten anzeigt. Wenn Sie die Grenzwerte überschreiten, kann es sein, dass GitLab auf Sie zukommt und Sie dabei unterstützt, Ihre Nutzung zu reduzieren oder zusätzliche Add-ons zu erwerben. In Zukunft werden die Grenzwerte automatisch durchgesetzt. Dies wird gesondert bekannt gegeben.
          - question: Wo kann ich meine Speicher- und Übertragungsnutzung sehen?
            answer: >-
              Die Speichernutzung kann auf der Seite Einstellungen->Nutzungskontingent der Gruppe eingesehen werden. Die Verwendung von Übertragungen und Container-Registry wird zu einem späteren Zeitpunkt hinzugefügt.
          - question: Was wird auf meinen Grenzwert angerechnet?
            answer: >-
              Speicherbegrenzungen werden auf der obersten Ebene des Namensraums angewendet. Es wird der gesamte Speicherplatz gezählt, der von Projekten innerhalb eines Namensraums verbraucht wird, einschließlich der Git-Repositorys, des LFS, der Paketregistrierungen und der Artefakte.
          - question: Kann zusätzlicher Speicher und Transfer erworben werden?
            answer: >-
              Zusätzlicher Speicherplatz und zusätzliche Übertragungskapazität können als Add-on erworben werden. In unserer [Dokumentation]](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#purchase-more-storage){data-ga-name="documentation" data-ga-location="body"} finden Sie eine Beschreibung des Verfahrens.

      - header: "Compute-Einheiten"
        data_toggle: Compute minutes
        questions:
          - question: Was sind Compute-Einheiten?
            answer: >-
              Compute-Einheiten sind die Ausführungszeit (in Minuten) für Ihre Pipelines auf unseren gemeinsam genutzten Runnern. Die Ausführung auf Ihren eigenen Runnern erhöht nicht die Anzahl Ihrer Compute-Einheiten und ist unbegrenzt.
          - question: Was passiert, wenn ich meine Minutenbegrenzung erreiche?
            answer: >-
              Wenn Sie Ihre Grenzwerte erreichen, können Sie [Ihre Compute-Nutzung verwalten](/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage){data-ga-name="manage minutes usage" data-ga-location="body"}, [zusätzliche Compute-Einheiten erwerben](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#purchase-additional-ci-minutes){data-ga-name="purchase additional minutes" data-ga-location="body"} oder Ihr Konto auf Premium oder Ultimate upgraden. Sie können Ihre eigenen Runner auch dann noch benutzen, wenn Sie Ihren Grenzwert erreicht haben.
          - question: Gilt die Minutenbegrenzung für alle Runner?
            answer: >-
              Nein. Wir werden Ihre Minuten nur für unsere gemeinsam genutzten Runner (nur SaaS) beschränken. Wenn Sie einen [speziellen Runner für Ihre Projekte eingerichtet](https://docs.gitlab.com/runner/){data-ga-name="runner" data-ga-location="body"}haben, gibt es keinen Grenzwert für Ihre Build-Zeit auf GitLab SaaS.
          - question: Wird die Minutenbegrenzung in Abhängigkeit von der Anzahl der Benutzer in der Gruppe erhöht?
            answer: >-
              Nein. Der Grenzwert wird auf eine Gruppe angewendet, unabhängig von der Anzahl der Benutzer in dieser Gruppe.
          - question: Warum muss ich Kredit-/Debitkartendaten für kostenlose Compute-Einheiten eingeben?
            answer: >-
              Der Missbrauch der auf GitLab.com verfügbaren kostenlosen Compute-Einheiten zum Mining von Kryptowährungen hat massiv zugenommen, was zu zeitweiligen Leistungsproblemen für GitLab.com-Benutzer führt. Um einem solchen Missbrauch vorzubeugen, müssen Sie Ihre Kredit-/Debitkartendaten angeben, wenn Sie die gemeinsam genutzten Runner von GitLab.com nutzen möchten. Wenn Sie Ihren eigenen Runner verwenden oder gemeinsam genutzte Runner deaktivieren, sind keine Angaben zur Kredit-/Debitkarte erforderlich. Wenn Sie eine Karte angeben, wird diese mit einer Autorisierung im Wert von einem Dollar überprüft. Es werden keine Gebühren erhoben und es wird kein Geld überwiesen. Weitere Informationen finden Sie [hier](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="crypto mining" data-ga-location="body"}.
          - question: Gibt es für öffentliche Projekte einen anderen Grenzwert für Compute-Einheiten?
            answer: >-
              Ja. Öffentliche Projekte, die nach dem 17.07.2021 erstellt wurden, erhalten folgende Zuteilung von [Compute-Einheiten](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#ci-pipeline-minutes){data-ga-name="units of compute" data-ga-location="body"}: Free – 50.000 Minuten, Premium – 1.250.000 Minuten, Ultimate – 6.250.000 Minuten.

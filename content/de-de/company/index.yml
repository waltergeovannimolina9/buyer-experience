---
  title: Über GitLab
  description: Erfahre mehr über GitLab und darüber, wie wir ticken.
  components:
    - name: 'solutions-hero'
      data:
        title: Über GitLab
        subtitle: Hinter den Kulissen der DevSecOps-Plattform
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/company/company_hero.png
          image_url_mobile: /nuxt-images/company/company_hero.png
          alt: "GitLab-Teammitglieder"
          bordered: true
          rectangular: true
    - name: 'copy-about'
      data:
        title: "Was wir tun"
        subtitle: "Wir sind das Unternehmen hinter GitLab, der umfassendsten DevSecOps-Plattform."
        description: |
          Was 2011 als Open-Source-Projekt begann, um einem Programmier-Team die Zusammenarbeit zu erleichtern, ist heute eine Plattform, die Millionen von Menschen nutzen, um Software schneller und effizienter zu entwickeln und gleichzeitig die Sicherheit und Compliance zu verbessern.
          
          Seit unseren Anfängen glauben wir fest an Remote-Arbeit, Open Source, DevSecOps und Iteration. Wir stehen jeden Morgen (oder wann auch immer unser Arbeitstag beginnt) auf und loggen uns ein, um gemeinsam mit der GitLab-Community jeden Monat neue Innovationen zu entwickeln, die Teams dabei helfen, sich auf die schnellere Bereitstellung von großartigem Code zu konzentrieren und nicht auf ihre Toolchain.
        cta_text: 'Erfahre mehr über GitLab'
        cta_link: '/why-gitlab/'
        data_ga_name: "about gitlab"
        data_ga_location: "body"
    - name: 'copy-numbers'
      data:
        title: GitLab in Zahlen
        rows:
          - item:
            - col: 4
              title: |
                **Code**
                 **Mitwirkende**
              number: "über 3.300"
              link: http://contributors.gitlab.com/
              data_ga_name: contributors
              data_ga_location: body
            - col: 4
              title: |
                **Büros**
                Seit unserer Gründung vollständig remote
              number: "0"
          - item:
            - col: 4
              title: |
                **Kontinuierliche Releases**
                 am 22. eines jeden Monats
              number: "133"
              link: /releases/
              data_ga_name: releases
              data_ga_location: body
            - col: 4
              title: |
                **Teammitglieder**
                 in über 60 Ländern
              number: "über 1.800"
              link: /company/team/
              data_ga_name: team
              data_ga_location: body
          - item:
            - col: 8
              title: '**Geschätzte registrierte Benutzer(innen)**'
              number: "Über 30 Millionen"
        customer_logos_block:
          showcased_enterprises:
            - image_url: "/nuxt-images/home/logo_tmobile_white.svg"
              link_label: Link zur T-Mobile- und GitLab-Webcast-Startseite"
              alt: "T-Mobile-Logo"
              url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
            - image_url: "/nuxt-images/home/logo_goldman_sachs_white.svg"
              link_label: Link zur Kundenfallstudie von Goldman Sachs
              alt: "Goldman-Sachs-Logo"
              url: "/customers/goldman-sachs/"
            - image_url: "/nuxt-images/home/logo_cncf_white.svg"
              link_label: Link zur Kundenfallstudie der Cloud Native Computing Foundation
              alt: "Cloud-Native-Logo"
              url: /customers/cncf/
            - image_url: "/nuxt-images/home/logo_siemens_white.svg"
              link_label: Link zur Kundenfallstudie von Siemens
              alt: "Siemens-Logo"
              url: /customers/siemens/
            - image_url: "/nuxt-images/home/logo_nvidia_white.svg"
              link_label: Link zur Kundenfallstudie von Nvidia
              alt: "Nvidia-Logo"
              url: /customers/nvidia/
            - image_url: "/nuxt-images/home/logo_ubs_white.svg"
              link_label: Link zum Blogpost Wie UBS mit GitLab ihre eigene DevOps-Plattform geschaffen hat
              alt: "UBS-Logo"
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        cta_title: |
          Von der Planung bis zur Produktion
           – führe Teams in einer Anwendung zusammen
        cta_text: "Kostenlose Textversion anfordern"
        cta_link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
        data_ga_name: "free trial"
        data_ga_location: "body"
    - name: 'copy-mission'
      data:
        title: "Die Mission von GitLab"
        description: "Wir möchten erreichen, dass **jeder etwas beitragen kann**. Wenn alle einen Beitrag leisten können, werden die Benutzer(innen) zu Mitwirkenden und die Innovationsrate kann erheblich gesteigert werden."
        cta_text: 'Mehr erfahren'
        cta_link: '/company/mission/'
        data_ga_name: "mission"
        data_ga_location: "body"
    - name: 'showcase'
      data:
        title: Die Werte von GitLab
        items:
          - title: Zusammenarbeit
            icon:
              name: collaboration-alt-4
              alt: Zusammenarbeitsymbol
              variant: marketing
            text: "Wir legen Wert auf Dinge, die uns dabei helfen, effektiv zusammenzuarbeiten, wie z. B. positive Absichten anzunehmen, „Danke“ und „Entschuldigung“ zu sagen und zeitnahes Feedback zu geben."
            link:
              href: /handbook/values/#collaboration
              data_ga_name: Collaboration
              data_ga_location: body
          - title: Ergebnisse
            icon:
              name: increase
              alt: Erhöhen-Symbol
              variant: marketing
            text: "Wir arbeiten mit einem Sinn für Dringlichkeit und einem Hang zum Handeln, und wir halten, was wir versprechen – uns selbst, aber auch Kunden, Benutzer(innen) und Investor(innen) gegenüber."
            link:
              href: /handbook/values/#results
              data_ga_name: results
              data_ga_location: body
          - title: Effizienz
            icon:
              name: digital-transformation
              alt: Effizienz-Symbol
              variant: marketing
            text: "Von der Auswahl langweiliger Lösungen bis hin zur Dokumentation von allem und der Selbstverwaltung streben wir danach, bei den richtigen Themen schnell voranzukommen."
            link:
              href: /handbook/values/#efficiency
              data_ga_name: efficiency
              data_ga_location: body
          - title: "Vielfalt, Inklusion & Zugehörigkeit"
            icon:
              name: community
              alt: Community-Symbol
              variant: marketing
            text: "Wir setzen uns dafür ein, dass GitLab ein Ort ist, an dem sich Menschen mit unterschiedlichem Hintergrund und aus unterschiedlichen Verhältnissen zugehörig fühlen und sich entfalten können."
            link:
              href: "/handbook/values/#diversity-inclusion"
              data_ga_name: diversity inclusion
              data_ga_location: body
          - title: Iteration
            icon:
              name: continuous-delivery
              alt: Symbol für kontinuierliche Lieferung
              variant: marketing
            text: "Unser Ziel ist es, die kleinste brauchbare und wertvolle Sache zu erstellen und sie schnell zu veröffentlichen, damit wir Feedback erhalten."
            link:
              href: "/handbook/values/#iteration"
              data_ga_name: iteration
              data_ga_location: body
          - title: Transparenz
            icon:
              name: open-book
              alt: Buch-Symbol
              variant: marketing
            text: "Alles, was wir tun, ist standardmäßig öffentlich – von unserem Firmenhandbuch bis zu den Ticket-Trackern für unser Produkt."
            link:
              href: "/handbook/values/#transparency"
              data_ga_name: transparency
              data_ga_location: body
    - name: 'zeitleiste'
      data:
        title: "GitLab im Laufe der Jahre"
        items:
          - year: "2011"
            description: "Das GitLab-Projekt begann mit einem Commit"
          - year: ""
            description: "Wir haben begonnen, am 22. eines jeden Monats eine neue Version von GitLab zu veröffentlichen."
          - year: "2012"
            description: "Die erste Version von GitLab CI entsteht."
          - year: "2014"
            description: "GitLab wird gegründet."
          - year: "2015"
            description: "Beitritt zu Y Combinator und Veröffentlichung des GitLab-Handbuchs in unserem Website-Repository."
          - year: "2016"
            description: "Wir kündigen unseren Masterplan an und sammeln 20 Millionen Dollar in einer B-Finanzierungsrunde."
          - year: "2021"
            description: "GitLab Inc. wird ein börsennotiertes Unternehmen im Nasdaq Global Market (NASDAQ: GTLB)."
    - name: 'copy-about'
      data:
        title: "Arbeiten bei GitLab"
        description: |
          Wir streben danach, eine Remote-Umgebung aufzubauen, in der alle Teammitglieder auf der ganzen Welt ihr Bestes geben können und in der sie das Gefühl haben, dass ihre Stimme gehört wird und sie willkommen sind. Außerdem soll die Vereinbarkeit von Beruf und Privatleben wirklich im Vordergrund stehen.
          \
           Wenn du daran interessiert bist, ein Teil des Teams zu werden, laden wir dich ein, [mehr über die Arbeit bei GitLab](/jobs/) zu erfahren und dich auf offene Stellen zu bewerben, die gut zu dir passen könnten.
        cta_text: 'Alle offenen Stellen anzeigen'
        cta_link: '/jobs/all-jobs/'
        data_ga_name: "all jobs"
        data_ga_location: "body"
    - name: 'teamops'
      data:
        image: "/nuxt-images/company/company-teamops.svg"
        image_alt: "kollegen teilen dokumente"
        mobile_img: "/nuxt-images/company/TeamOps-mobile.svg"
        header_img: "/nuxt-images/company/TeamOps.svg"
        title: "Bessere Teams. Schnellerer Fortschritt. Eine bessere Welt."
        description: "TeamOps ist GitLabs einzigartige Personalpraxis, die Teamarbeit zu einer objektiven Disziplin macht. Auf diese Weise hat sich GitLab innerhalb eines Jahrzehnts von einem Startup zu einem globalen Unternehmen entwickelt. Durch eine kostenlose und leicht zugängliche Zertifizierung können andere Organisationen TeamOps nutzen, um bessere Entscheidungen zu treffen, Ergebnisse zu erzielen und unsere Welt voranzubringen."
        button:
            link: "/teamops/"
            text: "Erfahre mehr über TeamOps"
            data_ga_name: "learn more about teamops"
            data_ga_location: "body"
        link:
            link: "https://levelup.gitlab.com/learn/course/teamops"
            text: "Lass dich zertifizieren"
            data_ga_name: "get certified"
            data_ga_location: "body"
    - name: 'learn-more-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog-Symbol
              hex_color: "#171321"
            event_type: "Blog"
            header: "Erfahre, was es Neues bei GitLab, DevOps, Sicherheit und anderen Themen gibt."
            link_text: "Besuche den Blog"
            href: "/blog/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLab-Logo
            data_ga_name: "blog"
            data_ga_location: "body"
          - icon:
              name: announcement
              variant: marketing
              alt: Ankündigungssymbol
              hex_color: "#171321"
            event_type: "Presseraum"
            header: "Aktuelle Nachrichten, Pressemitteilungen und unsere Pressemappe."
            link_text: "Mehr erfahren"
            href: "/press/press-kit/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLab-Logo
            data_ga_name: "press kit"
            data_ga_location: "body"
          - icon:
              name: money
              variant: marketing
              alt: Geldsymbol
              hex_color: "#171321"
            event_type: "Investor Relations"
            header: "Die neuesten Aktien- und Finanzinformationen für Investoren."
            link_text: "Mehr erfahren"
            href: "https://ir.gitlab.com/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: GitLab-Logo
            data_ga_name: "investor relations"
            data_ga_location: "body"


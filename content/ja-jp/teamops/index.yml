---
  title: 'TeamOps: チームの効率性を最適化 | GitLab'
  og_title: 'TeamOps: Optimizing Team Efficiency | GitLab'
  description: TeamOpsは、迅速かつ効率的な戦略的実行を確実にするために意思決定のブロッカーを削減する、結果に焦点を当てたチーム管理規律です。 詳細はこちら!
  twitter_description: TeamOpsは、迅速かつ効率的な戦略的実行を確実にするために意思決定のブロッカーを削減する、結果に焦点を当てたチーム管理規律です。 詳細はこちら!
  og_description: TeamOpsは、迅速かつ効率的な戦略的実行を確実にするために意思決定のブロッカーを削減する、結果に焦点を当てたチーム管理規律です。 詳細はこちら!
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
      show: true
    title: |
      より良いチーム。
      より速い進歩。
      より良い世界。
    subtitle: チームワークを客観的な規律にする
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: Teamopsヒーローのイメージ
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: チームを登録する
      data_ga_name: enroll
      data_ga_location: hero
  spotlight:
    title: |
      TeamOpsは意思決定の速度を促進します
    subtitle: TeamOpsは、意思決定、情報、タスクをより効率的に管理することで、チームの生産性、柔軟性、および自律性を最大化するのに役立つ組織運用モデルです。
    description:
      "より良い意思決定のための環境を作り、それらの実行を改善することは、より良いチーム、そして最終的には進歩をもたらします。\n\n\n
       TeamOpsは、GitLabが10年でスタートアップからグローバルな上場企業にスケールアップした方法です。 現在、すべての組織に開放しています。"
    list:
      title: 一般的な問題点
      items:
        - 意思決定の遅れ
        - ミーティング疲れ
        - 内部コミュニケーションがとれていない
        - ハンドオフが遅く、ワークフローの遅延
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: チームをより良くする
      data_ga_name: make your team better
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
    title: |
      すべてのチームのために作られました。
      リモート、ハイブリッド、またはオフィス。
    image:
      url: /nuxt-images/team-ops/reasons-to-believe.png
      alt: Team Opsを信頼する理由のイメージ
    accordion:
      is_accordion: false
      items:
        - icon:
            name: group
            alt: User Group Icon
            variant: marketing
          header: TeamOpsは新しい目標であり、結果に焦点を当てた管理規律です
          text: チームメンバーがどのように関わり合っているかを運用可能な問題として扱うことで、組織がより大きな進歩を遂げるのに役立ちます。
        - icon:
            name: clipboard-check-alt
            alt: クリップボードチェックマークアイコン
            variant: marketing
          header: 実地試験を受けたシステム
          text: 私たちは過去5年間、GitLabでTeamOpsを構築し使用してきました。 結果は、組織の生産性が向上し、チームメンバーの仕事に対する満足度が向上しました。 それはここで作成されましたが、私たちはそれがほぼすべての組織を手助けできると信じています。
        - icon:
            name: principles
            alt: 継続的インテグレーションのイメージ
            variant: marketing
          header: 指導原則
          text: TeamOpsは、組織がダイナミックに変化する仕事の性質を合理的にナビゲートするのに役立つ4つの指針に基づいています。
        - icon:
            name: cog-user-alt
            alt: Cogユーザーアイコン
            variant: marketing
          header: アクションの原則
          text: 各原則は、一連の行動原則、つまりすぐに実施できる仕事のやり方に基づいた習慣によってサポートされています。
        - icon:
            name: case-study-alt
            alt: ケーススタディアイコン
            variant: marketing
          header:  現実の世界での例
          text: Gitlabで実践されているこの原則の実際の出力ベースの例のライブラリを増やして、行動原則を実現します。
        - icon:
            name: verification
            alt: リボンチェックアイコン
            variant: marketing
          header: TeamOpsコース
          text: チームと企業が共有環境でフレームワークを体験できるように、TeamOpsコースを通じてアラインメントを作成します。
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
    accordion_aos_animation: fade-left
    accordion_aos_duration: 1600
    aaccordion_os_offset: 200
  video_spotlight:
    title: |
      TeamOpsに没頭する
    subtitle: 世の中には、仕事の未来について多くの意見があります
    description:
      "TeamOpsコースは、チームメンバーがどのように関わり合っているかを運用可能な問題として扱うことで、組織がより大きな進歩を遂げるのに役立ちます。\n\n\n
       わずか数時間で、モデルの各指針と行動原則を深く掘り下げ、現在のチームの運用方法との互換性を評価し始めることができます。"
    video:
      url: 754916142?h=56dd8a7d5d
      alt: Teamopsヒーローのイメージ
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: 今すぐ登録
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      TeamOpsの指導原則
    subtitle: 組織は、人とチーム、すなわち創造性、視点、人間性を必要としています。
    cards:
      - title: 現実を共有
        ga_name: Shared reality
        description: |
          他の経営哲学では、知識の転送速度を優先していますが、TeamOpsでは知識の取得速度を最適化しています。
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: 詳細はこちら
          url: /handbook/teamops/shared-reality/
        color: '#FCA326'
      - title: だれもが貢献します
        ga_name: Everyone contributes
        description: |
          組織は、レベル、機能、または場所に関係なく、だれもが情報を使用して貢献できるシステムを作成する必要があります。
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: 詳細はこちら
          url: /handbook/teamops/everyone-contributes/
        color: '#966DD9'
      - title: 意思決定の速度
        ga_name: Decision velocity
        description: |
          成功は意思決定の速度と相関しています。つまり、特定の時間内に行われた意思決定の量と、より速い進歩から生じる結果です。
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: 詳細はこちら
          url: /handbook/teamops/decision-velocity/
        color: '#FD8249'
      - title: 測定の明瞭さ
        ga_name: Measurement clarity
        description: |
          これは、正しいものを測定するということです。 TeamOpsの意思決定の原則は、実行して結果を測定する場合にのみ有用です。
        icon:
          name: target
          slp_color: surface-700
        link:
          text: 詳細はこちら
          url: /handbook/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      ムーブメントに参加する
    description:
      "TeamOpsは、意思決定、情報、タスクをより効率的に管理することで、チームの生産性、柔軟性、および自律性を最大化するのに役立つ組織運用モデルです。 \n\n\n
       増え続けるTeamOpsを実践している組織のリストに参加しましょう。"
    list:
      title: 一般的な問題点
      items:
        - アドホックワークフローにより、アライメントが妨げられる
        - DIY管理が機能障害を引き起こす
        - 通信インフラは事後的
        - コンセンサスへの執着がイノベーションを妨げる
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: 今すぐ登録
      data_ga_name: enroll
      data_ga_location: join the movement
    quotes:
      - text: デプロイ前のテストにより、製品をリリースする準備ができているという自信が高まりました。また、デリバリー頻度も多くなりました。
        author: John Lastname
        note: 会社名、役職
      - text: 展開前のテストにより、製品をリリースする準備ができているという自信が高まりました。また、配信頻度も高くなりました。
        author: John Lastname
        note: 会社名、役職
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: クラウドネイティブロゴ
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: クラウドネイティブロゴ
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachsロゴ
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Siemensロゴ
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: クラウドネイティブロゴ
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: KnowBe4ロゴ
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: クラウドネイティブロゴ
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: クラウドネイティブロゴ
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachsロゴ
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
